(function () {
    function userMembership ($rootScope, localStorage, $state, $timeout, API_Service, constantAuthenticationAPI) {

        if  (data.iframeFormsAuthentication) {

            window.addEventListener ('message', function (event) {

                if (event.data.redirectUrl) {
                    window.open (event.data.redirectUrl);
                }

                if (event.data.is_logged_in) {
                    if (!data.authenticationByPage) {
                        $rootScope.$emit ('hideIframe');
                        $rootScope.$emit ('loggedIn', event.data);
                    }
                }

                // Auto Updates the height of the iframe.
                if (event.data.height) {
                    $ ('#' + event.data.id).css ('height', (event.data.height + 20) + 'px');
                }

            }, false);

        }

        $rootScope.redirectionPage = (data.redirectionPage) ? data.redirectionPage : '';

        $rootScope.redirectLastPageUsed = '';

        // ***** USER CREATION *****
        $rootScope.user = $rootScope.user || {};

        if (localStorage.getLocalStorage ('first_name')) {
            $rootScope.user.first_name = localStorage.getLocalStorage ('first_name');
        }

        if (localStorage.getLocalStorage ('last_name')) {
            $rootScope.user.last_name = localStorage.getLocalStorage ('last_name');
        }

        // ***** LOCALSTORAGE *****
        // on Reload, if the logged in was there, send directive that the user is logged in.
        if (localStorage.getLocalStorage ('logged_in')) {
            localStorage.setLocalStorage ('logged_in', true);
            $rootScope.isLogged = true;
        }

        // Listener used for logging in functionality
        $rootScope.$on ('loggedIn', function (e, f) {

            if (f != null) {
                localStorage.setLocalStorage ('logged_in', true);

                if (f.access_token) {
                    localStorage.setLocalStorage ('access_token', f.access_token);
                }

                if (f.refresh_token) {
                    localStorage.setLocalStorage ('refresh_token', f.refresh_token);
                }

                // Set the isLogged variable to know when the user is logged in or not.
                $rootScope.isLogged = true;

                // Need to be setted in parameters.js
                if (typeof data.getUserDataAfterLogin != 'undefined' && data.getUserDataAfterLogin == true) {
                    $rootScope.$emit('getUserData');
                }

                $timeout (function () {
                    $rootScope.$emit ('checkAuthenticationByPage', $state.params.identifier);
                }, 0);
            }

        });

        $rootScope.$on('getUserData', function () {
            $timeout(function () {

                API_Service(constantAuthenticationAPI.getUserData, {}).get_no_array({}, {},
                    function success(data) {

                        if (data.first_name) {
                            $rootScope.user.first_name = data.first_name;
                            localStorage.setLocalStorage ('first_name', data.first_name);
                        }

                        if (data.last_name) {
                            $rootScope.user.last_name = data.last_name;
                            localStorage.setLocalStorage ('last_name', data.last_name);
                        }

                    },

                    function error(data) {
                    });

            }, 0);
        });



        // ***** LOGOUT FUNCTION *****
        $rootScope.logOut = function () {
            $rootScope.$emit ('logOut');
        };

        // ***** LOGOUT LISTENER *****
        $rootScope.$on ('logOut', function () {

            localStorage.setLocalStorage ('logged_in', false);
            localStorage.setLocalStorage ('access_token', '');
            localStorage.setLocalStorage ('refresh_token', '');
            localStorage.setLocalStorage ('first_name', '');
            localStorage.setLocalStorage ('last_name', '');
            localStorage.setLocalStorage ('credits_amount', 0);

            $rootScope.isLogged = false;

            // Check the authentication of the page
            $rootScope.$emit ('checkAuthenticationByPage', $state.params.identifier);

            $rootScope.$emit('hideAuthenticationPages');

        });

        // ***** REDIRECTS *****

        // Event Listener to redirect to Dashboard
        $rootScope.$on ('redirectAfterLogin', function () {
            var redirectUrl = ($rootScope.redirectLastPageUsed != '') ? $rootScope.redirectLastPageUsed : $rootScope.redirectionPage;

            redirectObject = ($rootScope.redirectLastPageUsed != '') ? $rootScope.redirectLastPageObject : {};

            $state.transitionTo(redirectUrl, redirectObject, {
                notify: true,
                location: true
            });
        });

        // Event Listener to redirect to Login page
        $rootScope.$on ('redirectLogin', function () {
            $state.transitionTo(data.loginIdentifier, {}, {
                notify: true,
                location: 'replace'
            });
        });

        // Event Listener to redirect to Login page
        $rootScope.$on ('publicPage', function () {
            $state.transitionTo($state.params.identifier, {}, {
                notify: true,
                location: 'replace',
                reload: true
            });
        });


        // ***** TYPE OF PAGES *****
        //
        //      onlyPrivatePage
        //      onlyNonPrivatePage
        //      public
        //
        // *************************

        // Event Listener to show only Private pages
        $rootScope.$on ('onlyPrivatePage', function () {
            $rootScope.redirectLastPageUsed = $state.params.identifier;
            $rootScope.redirectLastPageObject = {};

            if ($state.params.id != undefined) {
                $rootScope.redirectLastPageObject.id = $state.params.id;
            }

            if ($state.params.slug != undefined) {
                $rootScope.redirectLastPageObject.slug = $state.params.slug;
            }

            if (!$rootScope.isLogged) {
                $rootScope.$emit ('redirectLogin');
            }
        });

        // Event Listener to show only non Private pages
        $rootScope.$on ('onlyNonPrivatePage', function () {

            // If the user is logged in, redirect to RedirectionPage
            if ($rootScope.isLogged) {
                if ($state.params.identifier != $rootScope.redirectionPage) {
                    $rootScope.$emit ('redirectAfterLogin');
                }
            }
        });

        $rootScope.$on ('checkAuthenticationByPage', function (e, pageName) {
            if (data.onlyNonPrivatePage.indexOf (pageName) != -1) {
                $rootScope.$emit ('onlyNonPrivatePage');
            } else if (data.onlyPrivatePage.indexOf (pageName) != -1) {
                $rootScope.$emit ('onlyPrivatePage');
            } else {
                //$rootScope.$emit ('publicPage');
            }
        });

        // ***** CHECK TYPE OF PAGE *****
        //
        //      onlyPrivatePage
        //      onlyNonPrivatePage
        //      public
        //
        // *************************


        $rootScope.$on ('$stateChangeSuccess', function (e, toState, toParams, fromState, fromParams) {
            $timeout(function () {
                $rootScope.$emit ('checkAuthenticationByPage', toState.name);
            }, 0);

        });

    }

    userMembership.$inject = ['$rootScope', 'localStorage', '$state', '$timeout', 'API_Service', 'constantAuthenticationAPI'];

    angular
        .module ('bravoureAngularApp')
        .run (userMembership);

}) ();
