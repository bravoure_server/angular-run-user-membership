
# Header Template - Bravoure Component

This Component it is used to load the logic behind the authentication

## Local Storage

after login some field are saved:
    
    logged_in
    access_token
    refresh_token
    first_name
    last_name
    

## Listeners

### loggedIn
    $rootScope.$on ('loggedIn', function () { ... }
  
The LocalStorage is working to store the data.

### redirectAfterLogin

    $rootScope.$on ('redirectAfterLogin', function () { ... }

The page is redirected to the $rootScope.redirectionPage 
that is store in parameters.js as "redirectionPage"
    
    
    // parameters.js 
    var data = {
        ....
        // redirectionPage used after logging in
        redirectionPage: 'top.dashboard_page',
        ... 
    }

### redirectLogin
Redirects to the login page, also using the "loginidentifier" in parameters.js

    $rootScope.$on ('redirectLogin', function () { ... }
    
    // parameters.js 
    var data = {
        ....
        // Identifier used for the login page
        loginIdentifier: 'top.login_page'
        ... 
    }
    
### checkAuthenticationByPage
Used to define the pages that need to be private or non private, also defined in parameters.js

    $rootScope.$on ('checkAuthenticationByPage', function () { ... }

    // parameters.js 
    var data = {
        ....
        // Authentication: Type of page
        onlyNonPrivatePage: ['top.login_page', 'top.sign_up_page'],
        onlyPrivatePage: ['top.dashboard_page'],
        ... 
    }
    
### onlyPrivatePage
    $rootScope.$on ('onlyPrivatePage', function () { ... }
    
If the user is not logged in, redirects to the Login page using "redirectLogin" event 


### onlyNonPrivatePage
    $rootScope.$on ('onlyNonPrivatePage', function () { ... }
    
If the user is logged in, redirects to the Redirect Page using "redirectAfterLogin" event 
    
    
### logOut
    $rootScope.$on ('logOut', function () { ... }

Cleans the localstorage and runs the "checkAuthenticationByPage" event



### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-run-user-membership": "~1.1"
      }
    }

and then run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    

