## Angular Run User Membership / Bravoure component

This Component it is used to load the logic behind the authentication
 
### **Versions:**

1.1.2 - Minor details
1.1.1 - Adding possiblity of getting User data after login
1.1.0 - Improved version of listener for logging in and signing up
1.0.0 - Initial version

---------------------
